//1. Project setup
1. clone project into your preferred IDEA
2. run the following command in the terminal - **npm install**

//2. Running the tests
You can run the tests in multiple ways:
1. To execute the tests in **BROWSER**, run the following command - <b>npx cypress open</b>
Then click on "search.spec.js" and a browser window should open and the test
process can be seen in the browser.
2. To execute tests in **HEADLESS** run the following command - <b>npx cypress run</b>
3. Docker is required for **PARALLEL** testing. If you have Docker working, then you can
trigger the parallel execution with the following command - **docker-compose up**

//3. Reporters
1. For singular execution the test report will be generated as an HTML document inside
the **cypress** folder called **reports**.
2. For parallel testing, multiple reports will be generated inside the **.results** folder
after parallel execution

//4. Running in a different browser
Cypress browser support - https://docs.cypress.io/guides/guides/launching-browsers#Firefox-Browsers
1. **BROWSER** - you can change to the browser of your choice when you've opened the window
for browser execution. Simply pick the desired browser from the **top-right** **dropdown** menu
and run the tests in a different browser.
2. **HEADLESS** - for this you can add a flag to the headless testing command. For example
**npx cypress run --browser chrome** or **npx cypress run --browser edge**.
3. **PARALLEL** - inside the **docker-compose.yml** file you can edit the flag in the commands
block and assign the desired browser.

//POM classes can be found in cypress/pages folder
