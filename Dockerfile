FROM cypress/included:9.4.1

COPY . /e2e
WORKDIR /e2e

RUN npm i --save-dev cypress-mochawesome-reporter
