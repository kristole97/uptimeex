import {Search} from "../../pages/Search";
import {ResultBlock} from "../../pages/ResultBlock";

const search = new Search();
const resultBlock = new ResultBlock();

const queryText = 'Opus Online'
const address = 'Pärnu mnt 141, 11314 Tallinn'
const phoneNbr = '682 9670'
const btnHrefValue = 'https://opusonline.co/'

describe('search result should have following parameters', ()=>{
    before(()=>{
        cy.visit('/')
        search.clearCookieWindow()
        search.searchTopic(queryText)
    })

    it('should have the company name', ()=>{
        resultBlock.validateResultName(queryText)
    })

    it('should have the correct address', ()=> {
        resultBlock.validateResultAddress(address)
    });

    it('should have the correct phone number', ()=> {
        resultBlock.validateResultPhone(phoneNbr)
    });

    it('should have navigate to website button', ()=> {
        resultBlock.validateWebsiteBtnFunctionality(btnHrefValue)
    });
})