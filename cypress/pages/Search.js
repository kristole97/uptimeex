export class Search {
    clearCookieWindow(){
        cy.get('#L2AGLb > .QS5gu').click()
    }

    searchTopic(input){
        cy.get('[name="q"]').type(input + '{enter}')
    }
}