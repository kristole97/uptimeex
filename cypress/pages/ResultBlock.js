export class ResultBlock {

    //method to get google result box element
    getResultBox(){
        return cy.get('#rhs')
    }

    //checks if valid result name exists
    validateResultName(name){
        return this.getResultBox().contains(name).should('have.text', 'Opus Online')
    }

    //checks if valid address exists
    validateResultAddress(address){
        return this.getResultBox().contains(address).should('have.text', address)
    }

    //checks if valid phone number exists
    validateResultPhone(phoneNumber){
        return this.getResultBox().contains(phoneNumber).should('have.text', phoneNumber)
    }

    //checks if 'Website' button is visible and
    //if button href attribute returns status code 200
    validateWebsiteBtnFunctionality(url){
        this.getResultBox().find('[href="' +url +'"]').should('be.visible')
        cy.request(url).then(resp => {
            expect(resp.status).to.eq(200)
        })
        return this
    }

}